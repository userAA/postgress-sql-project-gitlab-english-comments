gitlab page https://gitlab.com/userAA/postgress-sql-project-gitlab-english-comments.git
gitlab comment postgress-sql-project-gitlab-english-comments 

project postgres_sql_project (a full-fledged client-server application using a database postgress)
technologies used on the frontend:
    @material-ui/core,
    @material-ui/icons,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    formik,
    react,
    react-dom,
    react-query,
    react-router-dom,
    react-scripts,
    web-vitals,
    yup.

technologies used on the backend:
    bcrypt,
    cors,
    dotenv,
    express,
    jsonwebtoken,
    nodemon,
    pg,
    pg-hstore,
    sequelize.
	
	There is authorization and user registration. Authorized user can to create posts and to add comments to every from them.
All posts from all users is being represented on central page of project. One can change the taxt of post of authorized user.
Authorized user can to add likes to all posts and remove them from all posts. Added like from an authorized user is indicated by a gray 
hand from an unauthorized by a white hand. In the bottom left corner of every post the number of likes to it from different users is indicated.
In the lower right corner of each post is the name of the user who created this post.
	At clickig on text of any post then we receiving full information about this post, including it comments and posibility of them
adding or removing. If this post was created by authorized user, then at output of full information about this post will appear the 
button of removing this post and it will be possible to change the content of this post if you double click on this content.
In the same way it is possible change the title of this post if it was created by an authorized user.
	At clicking on user name of any post then we receiving the full information about this user including his posts. 
If this user is authorized, then it will be possible to change the password of this user.
