import {createContext} from "react";

//define context, in which will wrap the state of authorized user
export const AuthContext = createContext("");