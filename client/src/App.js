import './App.css';
import {BrowserRouter as Router, Route, Routes, Link} from "react-router-dom";
//finding the main page of project
import Home from "./pages/Home";
//finding the page of creating posts by authorized user
import CreatePost from "./pages/CreatePost";
//finding the page of representing of individual post with creating of comments to it 
import Post from "./pages/Post";
//finding the page of user registration
import Registration from "./pages/Registration";
//finding the page of user authorization
import Login from "./pages/Login";
//finding the page of representing profile of individual post
import Profile from "./pages/Profile";
//finding the page of user password changing
import ChangePassword from "./pages/ChangePassword";
//finding context, in which we wrapping the state of authorized user
import {AuthContext} from "./helpers/AuthContext";

//finding hooks useState and useEffect
import {useState, useEffect} from 'react';
import axios from 'axios';

function App() {
  //tasking initial state of authorized user
  const [authState, setAuthState] = useState({
    username: "", 
    id: 0, 
    status: false
  });

  useEffect(() => {
    //carring out the request on existence of authorized user
    axios.get("http://localhost:3001/auth/auth", { 
      //finding token from localStorage and sending it to server under headers
      headers : {
        accessToken: localStorage.getItem('accessToken')
      }
    }).then((response) => {

      if (response.data.error) 
      {
        //the specified request was unsuccessful
        setAuthState({...authState, status: false});
      }
      else 
      {
        //the specified request is turned out to be successful, in the state of authorized user
        //we locating information about authorized user
        setAuthState({   
          //authorized user name
          username: response.data.username, 
          //authorized user ID
          id: response.data.id, 
          //authorized user status
          status: true
        });
      }
    })
  }, [authState]);

  //the function of removing information about authorized user from localStorage
  const logout = () => {
    //removing information about authorized user from localStorage
    localStorage.removeItem("accessToken");
    //clearing the state of user authorization
    setAuthState({username: "", id: 0, status: false});
  }

  return (
    <div className="App">
      {/*Wrapping the state of user authorization to context */}
      <AuthContext.Provider value={{authState, setAuthState}}>
        <Router>
          <div className="navbar">
            <div className="links">
            
              {!authState.status ? (
                //there is no of authorized user
                <>
                  {/*The refference to page of user authorization */}
                  <Link to="/login">Login</Link>
                  {/*The refference to page of user registration */}
                  <Link to="/registration">Registration</Link>
                </>
              ) : (
                <>
                  {/*The refference to main page of project */}
                  <Link to="/">Home Page</Link>
                  {/*THe refference to page of creating post */}
                  <Link to="/createpost">Create A Post</Link>
                </>
              )}
            </div>
            <div className="loggedInContainer">
              {/*Representing user authorization name, if it exists */}
              {authState.status && <h1>{authState.username}</h1>} 
              {/*The button of clearing information about authorized user, if it exists  */}            
              {authState.status && <button onClick={logout}>Logout</button>}
            </div>
          </div>
          <Routes>
            {/*Switching router to main page of project */}
            <Route path="/" element = {<Home/>} />
            {/*Switching router  to page of creating post */}
            <Route path="/createpost" element = {<CreatePost/>} />
            {/*Switching router to page of representing full information about post with ID id 
              including it comments and the time of them creating */}
            <Route path="/post/:id" element  = {<Post/>} />
            {/*Switching router to page of user registration */}
            <Route path="/registration" element  = {<Registration/>} />
            {/*Switching router to page of user authorization */}
            <Route path="/login" element = {<Login/>} />
            {/*Switching router to page of representing of profile information about user with ID id */}
            <Route path="/profile/:id" element = {<Profile/>} />
            {/*Switching router to page of changing user password */}
            <Route path="/changepassword" element = {<ChangePassword/>} />
          </Routes>
        </Router>
      </AuthContext.Provider>
    </div>
  );
}

export default App;