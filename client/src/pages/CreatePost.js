//finding hook useEffect
import React, {useEffect} from "react";
import {Formik, Form, Field, ErrorMessage} from "formik";
import * as Yup from 'yup';
import axios from 'axios';
//finding out hook useNavigate
import {useNavigate} from "react-router-dom";

//creating post function by authorized user
function CreatePost() {
    //defining the component of site navigation from hook useHistory
    let navigate = useNavigate();

    //the initial state of data for a post that is created by an authorized user
    const initialValues = {
        //empty name of creating post
        title : "",
        //empty content of creating post
        postText: ""
    }

    useEffect(() => {
        //there is no authorized user, switching to page of user authorization
        if (!localStorage.getItem("accessToken")) {
            navigate("/login");
        }
    }, [navigate])

    //the control schema of post data, that is created by an authorized user
    const validationSchema = Yup.object().shape({
        //the way verification of specified post name
        title: Yup.string().required("You must input a Title!"),
        //the way verification of specified post content
        postText: Yup.string().required()
    })

    const onSubmit = (data) => {
        //making up request to server for creating new post by authorized user
        axios.post("http://localhost:3001/posts", 
        //sending data for creating specified post
        data, 
        {
            //sending token that contains information about authorized user from localStorage
            headers: {accessToken: localStorage.getItem("accessToken")}
        }).then(() => {
            //switching to main project page
            navigate("/");
        })
    }

    return (
        <div className="createPostPage">
            <Formik 
                initialValues={initialValues} 
                onSubmit={onSubmit} 
                validationSchema={validationSchema}
            >
                <Form className="formContainer">
                    <label>Title: </label>
                    <ErrorMessage name="title" component="span"/>
                    {/*The field for setting the name of the post */}
                    <Field 
                        autocomplete="off"
                        id="inputCreatePost" 
                        name="title" 
                        placeholder="(Ex, John...)"
                    />
                    <label>Post: </label>
                    <ErrorMessage name="postText" component="span"/>
                    {/*The field for setting the content of the post itself */}
                    <Field 
                        autocomplete="off"
                        id="inputCreatePost" 
                        name="postText" 
                        placeholder="(Ex, Post...)"
                    />
                     {/*The button of specified posts creating  */}
                    <button type="submit">Create Post</button>
                </Form>
            </Formik>
        </div>
    )
}

export default CreatePost;