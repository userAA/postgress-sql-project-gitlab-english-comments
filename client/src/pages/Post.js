import React, {useEffect, useState, useContext} from "react";
//finding the hooks useParams and useNavigate
import {useParams, useNavigate} from 'react-router-dom';
import axios from 'axios';
//finding the context in which the state of authorized user is wrapped
import {AuthContext} from "../helpers/AuthContext";

//the page of showing of concreting post
function Post() 
{
    //finding representing post ID
    let {id} = useParams();
    //the state of post data
    const [postObject, setPostObject] = useState({});
    //the state of list comments to post
    const [comments, setComments] = useState([]);
    //the state of new comment to post
    const [newComment, setNewComment] = useState("");
    //the state of authorized user, received from context
    const {authState} = useContext(AuthContext);

    //defining the site navigation component from hook useHistory
    let navigate = useNavigate();

    useEffect(() => 
    {
        //making up request for downloading with server the information about post with ID id
        axios.get(`http://localhost:3001/posts/byId/${id}`).then((response) => 
        {
            //fixing the information downloading with server about post with ID id 
            setPostObject(response.data);
        })   
        //making up request for downloading with server of comments to post with ID id
        axios.get(`http://localhost:3001/comments/${id}`).then((response) => 
        {
            //fixing downloaded comments
            setComments(response.data);
        })   
    }, [id, setPostObject, setComments])

    //the function of adding new comment of represented post in comments data base of this post
    const addComment = () => 
    {
        //making up request on server for adding of new comments of represented post in comments data base of this post 
        axios.post("http://localhost:3001/comments", {
            //the content of comment
            commentBody: newComment, 
            //post ID
            postId: id
        },
        {
            //sending on server the token, which contents the information about authorized user from localStorage
            headers: {
                accessToken: localStorage.getItem("accessToken")
            }    
        })
        .then((response) => {
            if (response.data.error) {
                //the request failed, we output the information about error
                alert(response.data.error);
            }
            else
            {
                //fixing added comment
                const commentToAdd = {
                    //the text of comment
                    commentBody: newComment, 
                    //the user name which added comment
                    username: response.data.username,
                    //comment ID
                    id: response.data.id
                };
                //fixing list of all comments to represented post
                setComments([...comments, commentToAdd]);
                //Clearing the state of new comment
                setNewComment("");
            }
        })
    }

    //remove comment function
    const deleteComment = (id) => 
    {
        //making up request to server for removing from considering posts the comment with ID id
        axios.delete(`http://localhost:3001/comments/${id}`, 
        {
            //sending to server token, which contents the information about authorized user from localStorage
            headers: {
                accessToken: localStorage.getItem("accessToken")
            }
        })
        .then(() => 
        {
            //fixing new list of comments without removed comment
            setComments(
                comments.filter((val) => {
                    return val.id !== id;
                })
            )
        })
    }

    //removing post function
    const deletePost = (id) => {
        //making up to server for removing post with ID id
        axios.delete(`http://localhost:3001/posts/${id}`, {
            //removing to server token, which contents information about authorized user from localStorage
            headers: {
                accessToken: localStorage.getItem("accessToken")
            }
        }).then(() => {
            //we switching to main page of project
            navigate("/");
        })
    }

    //correction post function
    const editPost = (option) => {
        if (option === "title") {
            //correcting the name of post
            let newTitle = prompt("Enter New Title:");
            if (newTitle !== "" && newTitle !== undefined && newTitle !== null)
            {
                //the making up request to server for changing of post title with ID id
                axios.put("http://localhost:3001/posts/title", 
                {
                    //new post title
                    newTitle: newTitle, 
                    //post ID
                    id: id
                },
                //sending to server the token, which contents the information about authorized user from localStorage
                {
                    headers: {accessToken: localStorage.getItem("accessToken")}
                });
            }
            //fixing new post after changing it title
            setPostObject({...postObject, title: newTitle});
        } 
        else 
        {
            //correcting post itself
            let newPostText = prompt("Enter New Text:");
            if (newPostText !== "" && newPostText !== undefined && newPostText !== null)
            {
                //making up the request to server for changing of text content with ID id
                axios.put("http://localhost:3001/posts/postText", 
                {
                    //new post content
                    newText: newPostText, 
                    //post ID
                    id: id
                },
                //sending to server token, which contents the information about authorized user from localStorage
                {
                    headers: {accessToken: localStorage.getItem("accessToken")}
                });
            }
            //fixing new post after changing of it content
            setPostObject({...postObject, postText: newPostText});
        }
    }

    return (
        <div className="postPage">
            <div className="leftSide">
                <div className="post" id="individual">
                    <div 
                        className="title" 
                        onClick={() => {
                            if ( 
                                localStorage.accessToken !== undefined &&  
                                authState.username       !== "" &&
                                postObject.username      !== "" &&
                                authState.username       === postObject.username
                            ) 
			    {
                            //one can to change the post title, if post was created by authorized user
                                editPost("title");
                            }
                        }}
                    >
                        {/*Post title*/}
                        {postObject.title}
                    </div>
                    <div                         
                        className="body"
                        onClick={() => {
                            if ( 
                                localStorage.accessToken !== undefined &&  
                                authState.username       !== ""        &&
                                postObject.username      !== ""        &&
                                authState.username       === postObject.username
                            ) 
			    { 
                                //one can to change the post content, if post was created by authorized user
                                editPost("body")
                            }
                        }}
                    >
                        {/*post content*/}
                        {postObject.postText}
                    </div>
                    <div className="footer">
                        {/*User name, which created this post */}
                        {postObject.username}
                        {   localStorage.accessToken !== undefined &&  
                            authState.username       !== ""        &&
                            postObject.username      !== ""        &&
                            authState.username       === postObject.username && (
                            //The button of removing post, if post was created by authorized user
                            <button 
                                onClick={() => {
                                    deletePost(postObject.id)
                                }}
                            >
                                Delete Post
                            </button>
                        )}
                    </div>
                </div>
            </div>
            {  (localStorage.accessToken !== undefined &&  
                authState.username       !== ""        &&
                postObject.username      !== ""        &&
                authState.username       === postObject.username ) && (
                <div className="rightSide">
                    <div className="addCommentContainer">
                        {/*Settings field of new comments to post*/}
                        <input 
                            type="text"
                            placeholder="Comment..."
                            autoComplete="off"
                            value={newComment}
                            onChange={(event) => {  
                                setNewComment(event.target.value);
                            }}
                        />
                        {/*The button of adding new comment to post*/}
                        <button onClick={addComment}>Add Comment</button>
                    </div>
                    {/*Showing the list of all comments to represented post*/}
                    <div className="listOfComments">
                        {comments.map((comment, key) => {
                            return (
                                <div key={key} className="comment">
                                    {/*One af all comments*/}
                                    {comment.commentBody}
                                    {/*User name, which created this comment*/}
                                    <label>Username: {comment.username}</label>
                                    { /*One can to remove comment, if it was created by authorized user */}
                                    <button onClick={() => {deleteComment(comment.id)}}>X</button>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )}
        </div>
    )
}

export default Post;