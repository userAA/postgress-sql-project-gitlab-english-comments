import React from 'react';
//finding hook useHistory
import {useNavigate} from "react-router-dom";
import {Formik, Form, Field, ErrorMessage} from "formik";
import * as Yup from "yup";
import axios from "axios";

//user registration function
function Registration() {
    //the initial state of data for user registration
    const initialValues = {
        //empty user name
        username: "",
        //empty user password
        password: ""
    }

    //defining site navigation component from hook useHistory 
    let navigate = useNavigate();

    //the validation schema for user registration
    const validationSchema = Yup.object().shape({
        //in this way will control the name of the registered user
        username: Yup.string().min(3).max(15).required(),
        //in this way will control the password of the registered user
        password: Yup.string().min(4).max(20).required()
    })

    const onSubmit = (data) => {
        //implementing request for user registration 
        axios.post("http://localhost:3001/auth", data).then(() => {
            //in the case of successfull user registration going to on page of user authorizations
            navigate('/login');
        })
    };

    return (
        <div className="createPostPage">
            <Formik 
                initialValues={initialValues} 
                onSubmit={onSubmit} 
                validationSchema={validationSchema}
            >
                <Form className="formContainer">
                    <label>Username: </label>
                    <ErrorMessage name="username" component="span"/>
                    {/*Settings field of registered user name*/}
                    <Field 
                        autocomplete="off"
                        id="inputCreatePost" 
                        name="username" 
                        placeholder="(Ex, John...)"
                    />
                    <label>Password: </label>
                    <ErrorMessage name="password" component="span"/>
                    {/*Settings field of registered user password*/}
                    <Field 
                        autocomplete="off"
                        type="password"
                        id="inputCreatePost" 
                        name="password" 
                        placeholder="Your Password..."
                    />
                    {/*The button of starting process of user registration*/}
                    <button type="submit">Register</button>
                </Form>
            </Formik>
        </div>
    )
}

export default Registration;