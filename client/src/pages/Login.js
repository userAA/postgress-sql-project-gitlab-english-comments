import React, {useState, useContext} from 'react';
import axios from "axios";
//finding hook useHistory
import {useNavigate} from "react-router-dom";
//finding context of authorization
import {AuthContext} from "../helpers/AuthContext";

//user authorization page
function Login() {
    //initial state of the name of the authorized user
    const [username, setUsername] = useState("");
    //initial state of password of the authorized user
    const [password, setPassword] = useState("");
    //finding the function of settings state of user authorization according to context
    const {setAuthState} = useContext(AuthContext);

    //defining the site navigation component from hook useHistory
    let navigate = useNavigate();

    //user authorization function
    const login = () => {
        //the data according to authorized user
        const data = {username : username, password: password};
        //making up request for implementing of user authorization according to prepared data
        axios.post("http://localhost:3001/auth/login", data).then((response) => {
            if (response.data.error) 
            {
                //user authorization failed issue an error via alert
                alert(response.data.error);
            }
            else
            {
                //the user authorization is implemented
                localStorage.setItem("accessToken", response.data.token);
                //defining the state of authorized user
                setAuthState({
                    //the name of authorized user
                    username: response.data.username, 
                    //the ID of authorized user
                    id: response.data.id, 
                    //the status of authorization proccess wrapping in true
                    status: true
                });
                //switching to main page of project
                navigate("/");
            }
        })
    };
    
    return (
        <div className="loginContainer">
            <label>Username:</label>
            {/*Field for setting the name of the authorized user*/}
            <input 
                type="text" 
                onChange={(event) => {
                    setUsername(event.target.value)
                }}
            />
            <label>Password:</label>
             {/*Field for setting the password of the authorized user*/}
            <input 
                type="password" 
                onChange={(event) => {
                    setPassword(event.target.value)
                }}
            />
            {/*The button to start the user authorization process*/}
            <button  onClick={login}>Login</button>
        </div>
    )
}

export default Login;