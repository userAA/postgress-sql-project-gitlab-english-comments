//finding hooks useContext, useEffect and useState
import React, {useContext, useEffect, useState} from "react";
//finding hooks useParams and useHistory
import {useParams, useNavigate} from "react-router-dom";
import axios from "axios";
//finding context in which the state of authorized user was wrapped
import {AuthContext} from "../helpers/AuthContext";

//the page of creating profile information about user with it posts
function Profile() {
    //finding user ID
    let {id} = useParams();
    //defining site component navigation from hook useHistory
    let navigate = useNavigate();
    //user name state
    const [username, setUsername] = useState("");
    //list of user posts state
    const [listOfPosts, setListOfPosts] = useState([]);
    //the state of authorized user, received from context
    const {authState} = useContext(AuthContext);

    useEffect(() => {
        //making up request for receiving of main information about user with ID id
        axios.get(`http://localhost:3001/auth/basicInfo/${id}`)
        .then((response) => {
            //fixing user name with ID id
            setUsername(response.data.username);
        })

        //making request for receiving of posts of user with ID id
        axios.get(`http://localhost:3001/posts/byuserId/${id}`)
        .then((response) => {
            //fixing all posts of user with ID id
            setListOfPosts(response.data);
        })
    }, [id]);

    return (
        <div className="profilePageContainer">
            <div className="basicInfo">
                {/*Showing of user name*/}
                <h1>Username: {username}</h1>
                {authState.username === username && (
                    //Enabling the switching option to page of changing user password
                    //if we outputing of profile information about authorized user
                    <button 
                        onClick={() => {
                            navigate('/changepassword');
                        }}
                    >
                        Change My Password
                    </button>
                )}
            </div>
            <div className="listOfPosts">
                {/*Outputing the list of individual user posts*/}
                {listOfPosts.map((value, key) => {
                    return (
                        <div key={key} className="post">
                            {/*the title of individual post*/}
                            <div className="title"> {value.title} </div>
                                <div className="body"
                                    //the possibility of switching on page of individual post with ID value.id
                                    onClick={() => {
                                        navigate(`/post/${value.id}`);
                                    }}
                                >
                                {/*individual post content*/}
                                {value.postText}
                            </div>
                            <div className="footer">
                                 {/*the user name which created this post*/}
                                <div className="username"> {value.username} </div>
                                <div className="buttons">
                                    {/*the quantity of likes in this post*/}
                                    <label> {value.likes.length}</label>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>
        </div>
    )
}

export default Profile;