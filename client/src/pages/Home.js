//finding hook useContext
import React, {useContext} from "react";
import axios from "axios";
//finding hook useEffect and useState
import { useEffect, useState } from "react";
//finding hook useHistory
import { Link, useNavigate } from "react-router-dom";
import ThumbUpAltIcon from "@mui/icons-material/ThumbUpAlt";
//finding context in which the state of authorized user was wrapped
import {AuthContext} from "../helpers/AuthContext";

//the main page of project
function Home() {
    //the state of list posts according all users
    const [listOfPosts, setListOfPosts] = useState([]);
    //the state of list of posts related with likes according to authorized user
    const [likedPosts, setLikedPosts] = useState([]);
    //the state of authorized user, received from context
    const {authState} = useContext(AuthContext);

    //defining the site navigation component from the useHistory hook
    let navigate = useNavigate();

    useEffect(() => 
    {
        //If there is no information in localStorage about authorized user
        if (!localStorage.getItem("accessToken")) 
        {
            //switching to page of user authorization
            navigate("/login");
        } 
        else 
        {
            //making up request for receiving of all posts and likes of authorized user
            axios.get("http://localhost:3001/posts", 
            {
                //sending to server the token in which the information about authorized user from localStorage contains 
                headers: 
                { 
                    accessToken: localStorage.getItem("accessToken") 
                },
            })
            .then((response) => 
            {
                //received the list of all posts
                setListOfPosts(response.data.listOfPosts);
                //creating the list of posts related with likes according to authorized user
                setLikedPosts(
                    response.data.likedPosts.map((like) => 
                    {
                        return like.postId;
                    })
                );
            });
        }
    }, [navigate]);

    //the function of changing the quantity of likes according to posts 
    const likeAPost = (postId) => {
        //making up request on changing of quantity likes in data base of likes on server
        axios.post("http://localhost:3001/likes",
            {
                //the post ID, the like from which will be created or removed in data base of likes
                postId: postId 
            },
            {
                //sending on server the token in which contains the information about authorized user from localStorage
                headers: 
                {
                    accessToken: localStorage.getItem("accessToken")
                }
            }
        ).then((response) => 
        {
            //as a result of carrying out the specified request, we sort through all the posts
            setListOfPosts(listOfPosts.map((post) => 
            {         
                //ID of sorting through post post.id coincided with ID of processed post postId
                if (post.id === postId) 
                {   
                    if (response.data.liked) 
                    {
                        //like of processed post is defined and it is added in data base of likes
                        post = { ...post, likes: [...post.likes, 0] };
                        return post;
                    } 
                    else 
                    {
                        //the like of processed post was removed from it corresponding state and from data base of likes
                        const likesArray = post.likes;
                        likesArray.pop();
                        return { ...post, likes: likesArray };
                    }
                } 
                else 
                {
                    //overwise processed post is being remained without changes
                    return post;
                }
            }));

            if (likedPosts.includes(postId)) 
            {
                //if there is post with ID postId in list of posts related with likes according to authorized user
                //then it is being removed from this list
                 setLikedPosts(
                    likedPosts.filter((id) => 
                    {
                        return id !== postId;
                    })
                );
            } 
            else 
            {
                //overwise in the list of posts related with likes according to authorized user we add post with ID postId
                setLikedPosts([...likedPosts, postId]);
            }
        });
    };

    return (
        <div>  
            {/*Show all posts on this page */}           
            { authState.status ? ( listOfPosts.map((value, key) => {
                return (
                    <div key={key} className="post">
                        {/*Name of posts */}
                        <div className="title"> {value.title} </div>
                            <div className="body"
                                //The posibility switching to page of showing full information about post including it comments 
                                onClick={() => {
                                    navigate(`/post/${value.id}`);
                                }}
                            >
                            {/*Content of post */}
                            {value.postText}
                        </div>
                        <div className="footer">
                            <div className="username">
                                {/*The refference to page of showing user profile with ID value.userId, which created this post*/}
                                <Link to={`/profile/${value.userId}`}> {value.username} </Link>
                            </div>
                            <div className="buttons">
                                <ThumbUpAltIcon
                                    onClick={() => {
                                        //The function of adding or removing likes of this post on server in data base of likes
                                        likeAPost(value.id);
                                    }}
                                    className={
                                        //if there is like with ID of post value.id in data base of likes then we activate the button 
                                        //of removing of this like, overwise activate the button of adding of new like
                                        likedPosts.includes(value.id) ? "unlikeBttn" : "likeBttn"
                                    }
                                />
                                {/* The quantity of likes of an individual post*/}
                                <label> {value.likes.length}</label>
                            </div>
                        </div>
                    </div>
                );
                })) : 
                (
                    <>
                    </>
                )
            }
        </div>
    );
}

export default Home;