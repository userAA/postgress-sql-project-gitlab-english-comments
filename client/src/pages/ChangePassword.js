//finding hook useState
import React, {useState} from 'react'
import axios from "axios"

//user password change page
function ChangePassword () { 
    //old password
    const [oldPassword, setOldPassword] = useState("");
    //новый пароль
    //new password
    const [newPassword, setNewPassword] = useState("");

    //the function of password changing of authorized user
    const changePassword = () => {
        //implementing request for changing password according to authorized user
        axios.put("http://localhost:3001/auth/changepassword", {
            //old password
            oldPassword: oldPassword,
            //new password
            newPassword: newPassword
        }, 
        {
            //sending to server the token of authorized user from storage localStorage
            headers: {
                accessToken: localStorage.getItem("accessToken")
            }
        }).then((response) => {
            //the changing of password failed
            if (response.data.error) {
                alert(response.data.error);
            }
        })
    }

    return (
        <div>
            <h1>Change your password</h1>
            {/*The field for entering the old password of the authorized user */}
            <input 
                type="text" 
                placeholder="Old Password..."
                onChange={(event) => {
                    setOldPassword(event.target.value);
                }}
            />
            {/*The field for entering the new password of the authorized userr */}
            <input 
                type="text" 
                placeholder="Old Password..."
                onChange={(event) => {
                    setNewPassword(event.target.value);
                }}
            />
            {/*The starting up button of changing password of authorized user  */}
            <button onClick={changePassword}>Save Changes</button>
        </div>    
    )
}

export default ChangePassword;