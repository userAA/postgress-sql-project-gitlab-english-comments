import sequelize from "../db.js";
import DataTypes from "sequelize";
//finding the model of posts from database postgress 
import Posts from "./Posts.js";
//finding the model of likes from database postgress
import Likes from "./Likes.js";

//creating the user model and fixing it in database postgress
const Users = sequelize.define("users", {
    //user name
    username: {
        type: DataTypes.STRING, 
        allowNull: false
    },
    //user password
    password: {
        type: DataTypes.STRING, 
        allowNull: false
    }
})

//user model we relating with posts (ID of user exists in model of post)
Users.hasMany(Posts);
Posts.belongsTo(Users);

//user model we relating with likes (ID of user exists in model of like )
Users.hasMany(Likes);
Likes.belongsTo(Users);

//exporting model of user
export default Users;