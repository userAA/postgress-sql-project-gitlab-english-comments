import sequelize from "../db.js";
import DataTypes from "sequelize";

//creating the model of comments in data base postgress
const Comments = sequelize.define("comments", {
    //the content comments
    commentBody: { 
        type: DataTypes.STRING, 
        allowNull: false
    },
    //the user name, which created comments
    username: {
        type: DataTypes.STRING,
        allowNull: false
    }
})

//exporting model of comments
export default Comments;