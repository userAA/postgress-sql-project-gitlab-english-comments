import sequelize from "../db.js";
import DataTypes from "sequelize";
//finding the model of comments
import Comments from "./Comments.js";
//finding the model of likes
import Likes from "./Likes.js";

//creating the model of post and fixing it in data base postgress
const Posts = sequelize.define("posts", {
    //the name of posts
    title: { 
        type: DataTypes.STRING, 
        allowNull: false
    },
    //the content of posts
    postText: { 
        type: DataTypes.STRING, 
        allowNull: false
    },
    //the name of user, which creates post
    username: {
        type: DataTypes.STRING, 
        allowNull: false
    }
})

//model of post we relating with model of comments (ID of post exists in model of comments )
Posts.hasMany(Comments)
Comments.belongsTo(Posts)

//model of post we relating with model of likes (ID of post exists in model of likes )
Posts.hasMany(Likes)
Likes.belongsTo(Posts)

//exporting model of likes
export default Posts;