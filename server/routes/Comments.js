import express from "express";
const router = express.Router();
//finding the model of data base according to comments
import Comments from "../models/Comments.js";
//finding the function of defining data according to authorized user
import validateToken from "../middlewares/AuthMiddleware.js";

//the router of request for downloading with server of comments to post with ID postId
router.get('/:id', async (req, res) => {
    //defining ID of post according to which will download comments
    const postId = req.params.id;
    //defining comments according to posts with ID postId in data base of comments Comments
    const comments = await Comments.findAll({ where: { postId: postId } });
    //sending the defined comments to client-application of project
    res.json(comments);
})

//the router of request on server for adding of new comment in database of comments Comments
router.post("/", validateToken, async(req, res) => {
    //the content of new comment
    const newComment = req.body;  
    //the user name, which creates new comment
    const username = req.user.username;
    newComment.username = username;
    //creating specified comments in database of comments
    await Comments.create(newComment);
    //finding created comment from database of comments 
    const comment = await Comments.findOne({ where: { commentBody: newComment.commentBody } });
    //sending created comment to client-application of project
    res.json(comment);
})

//router request to server for removing of comment
router.delete("/:id", validateToken, async (req, res) => {
    //defining ID of removing comment
    const commentId = req.params.id;
    //we removing corresponding comment from data base of comments
    await Comments.destroy({
        where: {
            id: commentId
        }
    })
    //we fix a message that the required removing has been successfully completed
    res.json("DELETED SUCCESSFULLY");
})

export default router;