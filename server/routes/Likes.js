import express from "express";
const router = express.Router();
//finding the model of data base according to likes
import Likes from "../models/Likes.js";
//finding the function of defining data according to authorized user
import validateToken from "../middlewares/AuthMiddleware.js";

//router request for changing of quantity likes in database of likes on server
router.post("/", validateToken, async (req, res) => {
    //finding post ID
    const { postId } = req.body;
    //finding user ID
    const userId = req.user.id;

    //defining there is or no the like with ID post postId and ID user userId in data base of likes, 
    const found = await Likes.findOne({
        where: { postId: postId, userId: userId },
    });

    if (!found) 
    {
        //there is no specified like, then in database of likes we creating new like with ID of post postID and ID of user userId 
        await Likes.create({ postId: postId, userId: userId });
        //the result of succesfull creating of corresponding like sending to client-application project
        res.json({ liked: true });
    } 
    else 
    {
        //specified like exists, then in database of likes we removing this like with ID of post postID and ID of user userID
        await Likes.destroy({
            where: { postId: postId, userId: userId },
        });
        //the result of successfull removing of corresponding like sending to client-application of project
        res.json({ liked: false });
    }
})

export default router;