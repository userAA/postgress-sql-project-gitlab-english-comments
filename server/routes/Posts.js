import express from "express";
const router = express.Router();
//finding the model of database according to data of post
import Posts from "../models/Posts.js";
//finding model of data base according to data of like
import Likes from "../models/Likes.js";

//finding the function of defining data according to authorized user
import validateToken from "../middlewares/AuthMiddleware.js";

//router request for adding of all posts and likes of authorized user
router.get("/", validateToken, async (req, res) => {
    //receiving the list of all posts
    const listOfPosts = await Posts.findAll({include: [Likes]});
    //reseiving the needed list of likes with ID of authorized user req.user.id
    const likedPosts = await Likes.findAll({where: {userId: req.user.id}});
    //sending the result to client-application of project
    res.json({listOfPosts: listOfPosts, likedPosts: likedPosts});
});

//router request for downloading with server of information about post with ID id
router.get('/byId/:id', async (req, res) => {
    //finding post ID 
    const id = req.params.id;
    //defining information about post with ID id from all database Post
    const post = await Posts.findByPk(id);
    //sending to client-application the getting information about post with ID id
    res.json(post);
})

//router request for receiving of user posts with known ID
router.get('/byuserId/:id', async (req, res) => {
    //defining user ID, posts of which will find out from data base Posts
    const id = req.params.id;
    //finding all posts of user with ID id from database Posts
    const listOfPosts = await Posts.findAll({
        where: {userId: id},
        include: [Likes]
    });
    //sending all founded posts to client-application
    res.json(listOfPosts);
})

//router request for creating of new posts by authorized user
router.post("/", validateToken, async (req, res) => {
    //finding name and content of post
    const {title, postText} = req.body;
    //finding user name
    const username = req.user.username;
    //finding user ID
    const userId = req.user.id;
    //creating model of new post in database postgress Sql
    const post = await Posts.create({title, postText, username, userId});
    //sending result to client-application of project
    return res.json(post)
})

//router request to server for changing of post name
router.put("/title", validateToken, async (req, res) => {
    //finding post name, which will change and itself ID of post id 
    const {newTitle, id} = req.body;
    //changing the name of post with defined ID id
    await Posts.update({title: newTitle}, {where: {id: id}});
    //itself new name of post sending to client-application of project
    return res.json(newTitle);
})

//router request to server for changing of post content
router.put("/postText", validateToken, async (req, res) => {
    //finding the content of post which will change and itself ID post id
    const {newText, id} = req.body;
    //changing the content of post with defined ID id
    await Posts.update({postText: newText}, {where: {id: id}});
    //itself new content of post we sending to client-application of project
    return res.json(newText);
})

//router request to server for removing post from data base Posts
router.delete("/:id", validateToken, async (req, res) => {
    //finding ID of removing post
    const postId = req.params.id;
    //from data base according to posts Posts removing specified post
    await Posts.destroy({
        where: {
            id: postId
        }
    })
    //removing was successfully completed
    res.json("DELETED SECCESSFULLY");
})

export default router;