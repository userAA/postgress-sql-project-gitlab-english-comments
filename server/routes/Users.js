import express from "express";
const router = express.Router();
//finding the model of database according to user data
import Users from "../models/Users.js";
import bcrypt from "bcrypt";
//finding the function of defining data according to authorized user
import validateToken from "../middlewares/AuthMiddleware.js"; 
import jwt from "jsonwebtoken";

//router request according to user registration
router.post("/", async (req, res) => {
    //defining data of user registered
    const {username, password} = req.body;
    //encrypting password of user registered
    bcrypt.hash(password, 10).then((hash) => {
        //creating model of database of user registered in database Postgress sql
        Users.create({
            username: username,
            password: hash
        })
        //the user has been successfully registered
        res.json("SUCCESS");
    })
})

//router request according to authorized of user registered
router.post("/login", async (req, res) => {
    //definig data of authorized user
    const {username, password} = req.body;

    //seeking user with name username across the entire user database
    const user = await Users.findOne({where : {username: username}});

    //if there is no user, then outputing message that needed user did not found
    if (!user) res.json({error: "User Doesn't Exist"});

    //comparing the password password with password of defined user user
    bcrypt.compare(password, user.password).then((match) => {
        if (!match) res.json({error: "Wrong Username And Password Combination"});
        //creating user of authorized user according to his name user.username and ID user.id
        const accessToken = jwt.sign(
            {username: user.username, id: user.id}, 
            "importantsecret"
        );
        //user authorization has been implemented
        //sending to client-application all data about authorized user
        res.json({token: accessToken, username: username, id: user.id});
    })
})

//router request for verification of authorized user existence
router.get("/auth", validateToken, (req, res) => {
    res.json(req.user);
})

//router request for receiving of main information according to user with known ID
router.get("/basicInfo/:id", async (req, res) => {
    //defining the ID of interesting user
    const id = req.params.id;

    //finding in model of database according to users the information about user with ID id except his password
    const basicInfo = await Users.findByPk(id, {
        attributes: {exclude: ["password"]}
    })

    //sending found information to client-application
    res.json(basicInfo);
})

//the router according to request for changing password according to authorized user
router.put('/changepassword', validateToken, async (req, res) => {
    //finding old and new password
    const {oldPassword, newPassword} = req.body;
    //in all model of database according to users we finding data according to user with name req.user.username and password oldPassword
    const user = await Users.findOne({where: {username: req.user.username}});

    //comparing new password with old password 
    bcrypt.compare(oldPassword, user.password).then( async (match) => {
        if (!match) res.json({error: "Wrong Password Entered!"});

        //encrypting new password
        bcrypt.hash(newPassword, 10).then(async (hash) => {
            //in model of database of user with name req.user.username old encrypted password changing to new encrypted password 
            await Users.update({password: hash}, {where: {username: req.user.username}})
            res.json("SUCCESS");
        })
    })
})

export default router;