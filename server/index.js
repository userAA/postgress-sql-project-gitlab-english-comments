import express from "express";
import cors from "cors";

//defining data base tutorialDb in postgres_Sql
import sequelize from './db.js';

const app = express();
//the answer with routers will come in format json
app.use(express.json());
//including the posibility of transition with one local address to another local address 
app.use(cors());

//setting the server port
const PORT = process.env.PORT || 3001;

// Routers
import postRouter from "./routes/Posts.js";
//connecting routers of requests according to posts
app.use("/posts", postRouter);

import commentsRouter from "./routes/Comments.js";
//connecting routers of requests according to comments
app.use("/comments", commentsRouter);

import usersRouter from "./routes/Users.js";
//connecting routers of requests according to users
app.use("/auth", usersRouter);

import likesRouter from "./routes/Likes.js";
//connecting routers of requests according to likes
app.use("/likes", likesRouter);

const start = async () => {
    try {
        //we connecting to database tutorialDb in postgres_Sql
        await sequelize.authenticate();
        await sequelize.sync();
        
        //starting up server
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    }
    catch (e) {
        console.log(e);
    }
} 

start();
