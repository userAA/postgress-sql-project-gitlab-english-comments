import jwt from "jsonwebtoken";

// the verification function of existing of authorized user
const validateToken = (req, res, next) => {
    //finding token
    const accessToken = req.header("accessToken");
    
    //if there is empty token, then we outputing message, that there is no of authorized user
    if (!accessToken) return res.json({error: "User not logged in!"});

    try
    {
        //finding token according to key word importantsecret
        const validToken = jwt.verify(accessToken, "importantsecret");
        //it turned out to find token and create the data of authorized user
        req.user = validToken;
        if (validToken)
        {
            //let's move on corresponding routers who apply to function validateToken
            return next();
        }
    }
    catch (err)
    {
        //failed to find token and create data of authorized user
        return res.json({error: err});
    }
}

export default validateToken;